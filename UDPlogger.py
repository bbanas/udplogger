import argparse
import datetime
import time
import socket

localIP = "127.0.0.1"
bufferSize = 1024

def UDPLogger(filename, port):
    with open(filename, 'x') as f:
        f.writelines('LOGGING STARTED')
        f.write("\n")
        try:
            UDPServerSocket = socket.socket(family=socket.AF_INET, type=socket.SOCK_DGRAM)
            UDPServerSocket.bind((localIP, port))

            print("UDP server up and listening")
            while (True):
                bytesAddressPair = UDPServerSocket.recvfrom(bufferSize)
                message = bytesAddressPair[0]
                message = message.decode()
                print(len(message))
                print(message)
                # print(message[1])
                # print(message[2])
                # print(message[3])
                # print(message[4])
                # print(message[5])
                # print(message[6])

                time = datetime.datetime.now().strftime("%H:%M:%S")
                f.write(time)
                f.write(" ")
                f.write(message)
                f.write("\n")

                if (message == "EXIT"):
                    break
        except KeyboardInterrupt:
            f.close()
        except Exception as e:
            print (e)
            f.close()


parser = argparse.ArgumentParser()
parser.add_argument('-f', '--path', help='path to the log file', required=True)
parser.add_argument('-p', '--port', help='port to listen on', required=True)

args = parser.parse_args()

path = args.path
port = args.port

port = int(port)

print("-------- UDP logger for Boopszniks ----------")
print('Start saving log data in ' + path + ' file')
filename = path
UDPLogger(filename, port)

